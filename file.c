#include "abi.h"
#include "file.h"

inline int fstat_new(int fd, stat_t *statbuf);
inline int fstat_old(int fd, stat_t *statbuf);

int read(int fd, char* buf, int count) {
  return SYSCALL(0x3, fd, buf, count);
}

int write(int fd, const char* buf, int count) {
  return SYSCALL(0x04, fd, buf, count);
}


int open(const char* path, int flags, int mode) {
  return SYSCALL(0x05, path, flags, mode);
}

void close(int fd) {
  SYSCALL(0x6, fd);
}

int fstat_new(int fd, stat_t *statbuf) {
  return SYSCALL(0x6c, fd, statbuf);
}

int fstat_old(int fd, stat_t *statbuf) {
  return SYSCALL(0x1c, fd, statbuf);
}

int fstat(int fd, stat_t *statbuf) {
  return fstat_new(fd, statbuf);
}
