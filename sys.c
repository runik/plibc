#include "abi.h"

void exit(int exitcode) {
  SYSCALL(0x01, exitcode);
}

int execve(char *path, char **args, char **env) {
  return SYSCALL(0x0b, path, args, env);
}
