#ifndef __PLIBC_STR__H
#define __PLIBC_STR__H

int strlen(const char *s);
#endif
