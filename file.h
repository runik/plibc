#ifndef __PLIBC_FILE_H
#define __PLIBC_FILE_H
#include "types.h"

struct _old_kernel_stat {
	unsigned short st_dev;
	unsigned short st_ino;
	unsigned short st_mode;
	unsigned short st_nlink;
	unsigned short st_uid;
	unsigned short st_gid;
	unsigned short st_rdev;
	unsigned long  st_size;
	unsigned long  st_atime;
	unsigned long  st_mtime;
	unsigned long  st_ctime;
};

typedef struct _old_kernel_stat stat_t;

int open(const char* path, int flags, int mode);
size_t write(int fd, const char* buf, size_t count);
size_t read(int fd, char* buf, size_t count);
void close(int fd);

int fstat(int fd, stat_t* stb);

#endif
