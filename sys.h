#ifndef __PLIBC_SYS__H
#define __PLIBC_SYS__H

#define NULL 0x0

#define O_RDONLY   0
#define O_WRONLY   0
#define O_RDWR     2

/* Tiny elf has no main()
   also clean up the look of code
 */
#define entry _start /* entry to the program */


/* standard streams */
#define stdin  0
#define stdout 1
#define stderr 2

/* forward declations */
struct stat;


/* Linux system syscalls */
void exit(int exitcode);
int  execve(char *path, char **args, char **env);

#define major(dev)	((dev)>>8)
#define minor(dev)	((dev) & 0xff)
#define MKDEV(ma,mi)	((ma)<<8 | (mi))

/* string functions */
int strlen(char* s);

#endif
