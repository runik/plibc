#ifndef __ABI_H
#define __ABI_H

long __syscall1(int);
long __syscall2(int,int);
long __syscall3(int,int,int);
long __syscall5(int,int,int,int);
long __syscall6(int,int,int,int,int);

#define SYSCALL__(_1,_2,_3,_4,_5,_6,NAME,...) NAME

#define SYSCALL(...) SYSCALL__(__VA_ARGS__,			    \
			       __syscall6, __syscall5,		    \
			       __syscall4, __syscall3,		    \
			       __syscall2, __syscall1)(__VA_ARGS__)

#endif
